/******************************************************************************
* TASK: bugged4.c
* DESCRIPTION:
*   Very simple program with segmentation fault.
******************************************************************************/

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define N 1048

int main (int argc, char *argv[]) 
{
    int nthreads, tid, i, j;

    // array is too big to place `NUM_THREADS` of them on stack. One can either enlarge stack size in compile time,
    // or allocate this arrays on heap.
    // double a[N][N];

    #pragma omp parallel shared(nthreads) private(i, j, tid)
    {
        double *a = malloc(sizeof(*a) * N * N);

        tid = omp_get_thread_num();
        if (tid == 0) 
        {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
        }
        printf("Thread %d starting...\n", tid);

        for (i = 0; i < N; i++)
            for (j = 0; j < N; j++)
                a[i * N + j] = tid + i + j;

        // `a` is array of `double` values
        printf("Thread %d done. Last element= %lf\n", tid, a[N * N - 1]);
        free(a);
    } 
}
