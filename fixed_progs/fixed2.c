/******************************************************************************
* TASK: bugged2.c
* DESCRIPTION:
*   Another program with bug.
******************************************************************************/

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int nthreads, i, tid;
    float total;

    // fix sharing modes of external variables, assuming that total is the accumulator
    #pragma omp parallel default(none) private(nthreads, tid, i) shared(total)
    {
        tid = omp_get_thread_num();
        if (tid == 0)
        {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
            total = 0.0;
        }
        #pragma omp barrier
        printf("Thread %d is starting...\n", tid);
        #pragma omp barrier

        // add reduction clause to avoid data-race
        #pragma omp for schedule(dynamic, 10) reduction(+:total)
        for (i = 0; i < 1000000; i++)
            total = total + i*1.0;

        printf ("Thread %d is done! Total= %e\n", tid, total);
    }
}
